# Andrew's Docker Setup

	* Add your moodle files for each site into their respective folders, "avado", "dotnative", "cipd" in the project root folder
	* Copy config.example.php into each moodle directory and rename to 'config.php'
	* For each moodle installation add the site name to the db prefix i.e. "avado_mdl_", "dotnative_mdl_" etc... inside config.php
	* Also change the $CFG->wwwroot to match the moodle installs url as below (e.g. 'http://avado.local').
	* Add "avado.local", "dotnative.local" and "cipd.local" to you hosts file directing to 127.0.0.1
	* Open your console at the root of the project (docker, not the individual moodle site).
	* To start docker type 'docker-compose up -d'
	* To start docker type 'docker-compose down'
	* You can then access each moodle installation at their repsective url i.e. avado.local
