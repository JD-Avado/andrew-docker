# see https://github.com/cmaessen/docker-php-sendmail for more information

FROM php:7.2.0-fpm

RUN apt-get update \
    && apt-get install -y sudo \
    && apt-get install -y libpq-dev zlib1g-dev libicu-dev g++ libxml2-dev wget git unzip zip \
    && apt-get install -y libpng-dev \
    && apt-get install -y locales \
    && docker-php-ext-configure intl \
    && docker-php-ext-configure opcache --enable-opcache \
    && docker-php-ext-install pdo pdo_pgsql pgsql zip gd xmlrpc soap intl opcache \
    && apt-get clean \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && sed -i 's/# en_AU.UTF-8 UTF-8/en_AU.UTF-8 UTF-8/' /etc/locale.gen \
    && locale-gen en_AU.UTF-8 \
    && dpkg-reconfigure --frontend noninteractive locales


COPY config/ini/opcache.ini $PHP_INI_DIR/conf.d/opcache.ini

RUN pecl install redis && docker-php-ext-enable redis
&& echo "extension=redis.so" > /usr/local/etc/php/conf.d/redis.ini

RUN pecl install xdebug-2.5.5 intl \
    && echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "[XDebug]" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_log=/tmp/xdebug.log" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_autostart=on" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_connect_back=on" >> /usr/local/etc/php/conf.d/xdebug.ini

RUN echo "localhost localhost.localdomain" >> /etc/hosts